Rails.application.routes.draw do



  ########################################################################################################
  
  namespace :admin do
    
    controller :users do
    end
    resources :users
    
    controller :categorias do
    end
    resources :categorias
    
    controller :produtos do
    end
    resources :produtos
    
    controller :pages do
      get '/home' => :home
      get '/logs' => :logs
    end

    root 'pages#home'
    get '*path' => redirect('/')
  end

  ########################################################################################################

  devise_for :users, path: '', path_names: { sign_in: 'login', sign_out: 'logout', sign_up: "register"}
    
    controller :produtos do
    end
    resources :produtos, only: [:show, :index]

  controller :pages do
    get '/home' => :home
  end

  root 'pages#home'
  get '*path' => redirect('/')
end
