class Admin::CategoriasController < Admin::ApplicationController
    before_action :set_categoria, only: [:show, :edit, :update, :destroy]


	def index
		conditions = []
    conditions << "categorias.id IN (#{params[:id].split(/\W+/).uniq.join(', ')})" unless params[:id].blank?
    conditions << "categorias.nome LIKE '%#{params[:nome]}%'" unless params[:nome].blank?

		@categorias = Categoria.where(conditions.join(" AND ")).order("created_at desc")

	end

	def show
	end

	def edit
	end

	def update
    respond_to do |format|
        if @categoria.update(categoria_params)
            format.js {render inline: "M.toast({html: 'Atualizado!', classes: 'green'}); " }
        else
            format.js {render inline: "M.toast({html: 'Não foi possível editar a categoria #{@categoria.nome}', classes: 'red'}); " }
        end
    end
	end

	def new
		@categoria = Categoria.new
	end

	def create
		@categoria = Categoria.new(categoria_params)
		if @categoria.save
		    redirect_to edit_admin_categoria_path(@categoria), notice: "Categoria #{@categoria.nome} criado com sucesso!"
		else
	    respond_to do |format|
	        format.js
	    end
		end
	end

	def destroy
	end

	private

  def categoria_params
      params.require(:categoria).permit(:nome, :descricao, :tipos, :ativo)
  end

	def set_categoria
		@categoria = Categoria.find(params[:id].to_i)
	end

end
