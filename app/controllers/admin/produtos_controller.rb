class Admin::ProdutosController < Admin::ApplicationController
    before_action :set_produto, only: [:show, :edit, :update, :destroy]
      skip_before_action :verify_authenticity_token

    def index
        conditions = []
        conditions << "produtos.id IN (#{params[:id].split(/\W+/).uniq.join(', ')})" unless params[:id].blank?
        conditions << "produtos.nome LIKE '%#{params[:nome]}%'" unless params[:nome].blank?
        @produtos = Produto.where(conditions.join(" AND ")).order("created_at desc")
    end

    def show
    end

    def edit
    end

    def update
      if @produto.update(produto_params)
        if params[:produto][:imagem_capa].blank?
          respond_to do |format|
            format.js {render inline: "M.toast({html: 'Atualizado!', classes: 'green'}); " }
          end
        else
          redirect_to request.referrer, notice: "Atualizado"
        end
      else
        respond_to do |format|
          format.js
        end
      end
    end

    def new
    	@produto = Produto.new
    end

    def create
    	@produto = Produto.new(produto_params)
    	if @produto.save
            redirect_to edit_admin_produto_path(@produto), notice: "Produto #{@produto.nome} criado com sucesso!"
        else
            respond_to do |format|
                format.js
            end
        end
    end

    def destroy
      if @produto.modulos.size > 0
        Log.create(descricao: "Tentativa de deletar Produto!", objeto: {"id" => @produto.id, "tipo" => "produtos", "descricao" => "#{@produto.nome} - Tentativa de (#{current_user.id}) #{nome}", "class" => "orange", "user_id" => "#{current_user.id}"})
        respond_to do |format|
          format.js {render inline: "M.toast({html: 'Não foi possível deletar o produto #{@produto.nome}, módulos existentes.', classes: 'red'}); " }
        end
      else
        l = Log.new(descricao: "Produto deletado!", objeto: {"id" => @produto.id, "tipo" => "produtos", "descricao" => "#{@produto.nome} deletada por (#{current_user.id}) #{nome}", "class" => "red", "user_id" => "#{current_user.id}"})
        if @produto.destroy
          l.save
        else
          respond_to do |format|
            Log.create(descricao: "Tentativa de deletar Produto!", objeto: {"id" => @produto.id, "tipo" => "produtos", "descricao" => "#{@produto.nome} - Tentativa de (#{current_user.id}) #{nome}", "class" => "orange", "user_id" => "#{current_user.id}"})
            format.js {render inline: "M.toast({html: 'Não foi possível deletar o produto #{@produto.nome}, módulos existentes.', classes: 'red'}); " }
          end
        end 
      end
  end

  private

  def produto_params
      params.require(:produto).permit(:nome, :descricao, :preco, :desconto, :preco_final, :custo, 
                                      :ativo, :destaque, :categoria_id, {fotos: []})
  end

  def set_produto
   @produto = Produto.find(params[:id])
  end
end
