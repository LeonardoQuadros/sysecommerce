class Admin::UsersController < Admin::ApplicationController
    before_action :set_user, only: [:show, :edit, :update, :destroy]


	def index
		conditions = []
    conditions << "users.id IN (#{params[:id].split(/\W+/).uniq.join(', ')})" unless params[:id].blank?
    conditions << "users.nome LIKE '%#{params[:nome]}%'" unless params[:nome].blank?

		@users = User.where(conditions.join(" AND ")).order("created_at desc")
	end

	def show
	end

	def edit
	end

	def update
	end

	def new
		@user = User.new
	end

	def create
	end

	def destroy
	end

	private

	def set_user
		@user = User.find(params[:id].to_i)
	end

end
