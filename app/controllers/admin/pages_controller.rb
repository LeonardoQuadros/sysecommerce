class Admin::PagesController < Admin::ApplicationController
    def home
    end

    def logs
    	@logs = Log.all.order("created_at desc")
    end
end
