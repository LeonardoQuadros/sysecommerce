class ProdutosController < ApplicationController
    
    def index
        conditions = []
        unless params[:search].blank?
        	conditions << "((produtos.nome LIKE '%#{params[:search]}%') OR 
        									(produtos.descricao LIKE '%#{params[:search]}%') OR
        									(categorias.nome LIKE '%#{params[:search]}%') OR
        									(categorias.descricao LIKE '%#{params[:search]}%'))" 
      	end
      	conditions << "produtos.categoria_id = #{params[:categoria]}" unless params[:categoria].blank?
        @produtos = Produto.joins(:categoria).where(conditions.join(" AND ")).order("created_at desc")
    end

    def show
    	@produto = Produto.find(params[:id])
    end

end
