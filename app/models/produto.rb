class Produto < ApplicationRecord
  usar_como_dinheiro :preco, :preco_final, :custo

  validates :nome, :categoria, presence: true
  validates :nome, uniqueness: { scope: :categoria }

  belongs_to :categoria
  
  mount_uploaders :fotos, ImagemUploader

  after_create :log_for_create
  #after_update :log_for_update, if: -> { self.changes }

  def log_for_create
    Log.create(descricao: "Novo produto criada!", objeto: {"class" => "green", "id" => self.id, "tipo" => "produtos", "descricao" => "#{self.nome} - #{self.descricao}"})
  end

  def log_for_update
    Log.create(descricao: "Alteração na produto #{self.id} - #{self.nome}", objeto: {"id" => self.id, "tipo" => "user", "descricao" => "#{self.nome} - #{self.descricao}"}) 
  end

end

