class User < ApplicationRecord
  devise :database_authenticatable, :registerable,
  :recoverable, :rememberable, :validatable

  validates :nome, presence: true


  after_create :log_for_create
  #after_update :log_for_update, if: -> { self.changes }

  def log_for_create
    Log.create(descricao: "Novo usuário criado!", objeto: {"class" => "green", "id" => self.id, "tipo" => "users", "descricao" => self.nome})
  end

  def log_for_update
    Log.create(descricao: "Alteração no usuário #{self.id} - #{self.nome}", objeto: {"id" => self.id, "tipo" => "user", "descricao" => self.nome}) 
  end

end

