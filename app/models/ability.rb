class Ability
    include CanCan::Ability

    def initialize(user)
        user ||= User.new # guest user (not logged in)
        
        if user.admin == true
            can :manage, :all
        else
            can :manage,    Page
        end
    end
end
