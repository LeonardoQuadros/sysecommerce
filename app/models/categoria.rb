class Categoria < ApplicationRecord

  enum tipos: [:oculos, :relogios, :tenis]

  validates :nome, :tipos, presence: true
  validates :nome, uniqueness: { scope: :tipos }

  
  has_many :produtos

  after_create :log_for_create
  #after_update :log_for_update, if: -> { self.changes }

  def log_for_create
    Log.create(descricao: "Nova categoria criada!", objeto: {"class" => "green", "id" => self.id, "tipo" => "categorias", "descricao" => "#{self.nome} - #{self.descricao}"})
  end

  def log_for_update
    Log.create(descricao: "Alteração na categoria #{self.id} - #{self.nome}", objeto: {"id" => self.id, "tipo" => "user", "descricao" => "#{self.nome} - #{self.descricao}"}) 
  end

end

