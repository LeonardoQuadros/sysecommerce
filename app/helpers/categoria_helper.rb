module CategoriaHelper


	def categoria_tipos
    Categoria.tipos.map do |r, key|
     [I18n.t("categoria.tipos.#{r}"), r]
    end
	end

	def categorias_relogios
		Categoria.relogios
	end
	
	def categorias_tenis
		Categoria.tenis
	end
	
	def categorias_oculos
		Categoria.oculos
	end



end
