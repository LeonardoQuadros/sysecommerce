module ProdutoHelper

	def capa produto
		if produto.fotos.first.blank?
			if produto.categoria.oculos?
				return "/images/produtos/oculos/#{rand(1..10)}.jpg"
			elsif produto.categoria.relogios?
				return "/images/produtos/relogios/#{rand(1..10)}.jpg"
			elsif produto.categoria.tenis?
				return "/images/produtos/tenis/#{rand(1..10)}.jpg"
			end
		else
			return produto.fotos.fill_medium
		end
	end

	def relogios limite = 3
		Produto.where(categoria_id: Categoria.relogios.ids).limit(limite)
	end

	def tenis limite = 3
		Produto.where(categoria_id: Categoria.tenis.ids).limit(limite)
	end

	def oculos limite = 3
		Produto.where(categoria_id: Categoria.oculos.ids).limit(limite)
	end

	def relogios? produto
		produto.categoria.relogios?
	end

	def tenis? produto
		produto.categoria.tenis?
	end

	def oculos? produto
		produto.categoria.oculos?
	end


end