module AdminHelper

	def action_controller(controller)
		"#{controller.action_name}_#{controller.controller_name}"
	end
end
