class CreateProdutos < ActiveRecord::Migration[5.2]
  def change
    create_table :produtos do |t|
    	t.string :nome
    	t.string :descricao
    	t.decimal :preco
    	t.decimal :desconto
    	t.decimal :preco_final
    	t.decimal :custo
    	t.json 		:fotos
    	t.json 		:likes, default: {}
    	t.json 		:deslikes, default: {}
    	
    	t.boolean :ativo, default: false
    	t.boolean :destaque, default: false

    	t.references 			:categoria, index: true
    	t.timestamps
    end
  end
end
