class AddCamposComplementaresToUsers < ActiveRecord::Migration[5.2]
  def change
  	add_column :users, :nome, :string
  	add_column :users, :celular, :string
  	add_column :users, :data_nascimento, :string
  	add_column :users, :cpf, :string
  	add_column :users, :admin, :boolean, default: false
  	add_column :users, :carrinho, :json, default: {}
  	add_column :users, :logs, :json, default: {}
  	add_column :users, :ip_remote, :string
  	add_column :users, :user_agent, :string
  end
end
