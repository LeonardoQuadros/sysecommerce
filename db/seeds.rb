

Log.destroy_all
User.destroy_all

Faker::Config.locale = 'pt-BR'
user = User.create(nome: "Sys", email: "sys@sys.com.br", password: "sys", admin: true, cpf: "111.111.111-11", celular: "(11) 11111-1111")



Categoria.destroy_all
(1..3).each do |index|
	Categoria.create(nome: Faker::Lorem.word, descricao: Faker::Lorem.paragraph, ativo: true, tipos: 0)
	Categoria.create(nome: Faker::Lorem.word, descricao: Faker::Lorem.paragraph, ativo: true, tipos: 1)
	Categoria.create(nome: Faker::Lorem.word, descricao: Faker::Lorem.paragraph, ativo: true, tipos: 2)
end


Produto.destroy_all

Categoria.all.each do |categoria|
	(1..5).each do |index|
		preco = [1000, 1200, 1300, 1400, 500, 600, 550, 780, 840].sample
		desconto = [5, 10, 15, 20, 30].sample
		preco_final = (preco.to_f-(preco.to_f*((desconto.to_f/100)))).to_f
		Rails.logger.debug("  ============== preco:  #{preco} ")
Rails.logger.debug("  ============== desconto:  #{desconto} ")
Rails.logger.debug("  ============== preco_final:  #{preco_final} ")

		Produto.create(nome: Faker::Lorem.word, descricao: Faker::Lorem.paragraph, ativo: true, destaque: [true, false].sample, categoria_id: categoria.id, preco: preco, custo: [0, 0, 0, 100, 200, 300].sample, preco_final: preco_final, desconto: desconto)
	end
end